import 'dart:ffi';
import 'package:ffi/ffi.dart';
import 'dart:io';

final DynamicLibrary greeterNative = Platform.isAndroid
    ? DynamicLibrary.open("libgreeter.so")
    : Platform.environment.containsKey('FLUTTER_TEST')
    ? DynamicLibrary.open("rust/target/debug/libgreeter.so")
    : DynamicLibrary.process();


typedef GreetingFunction = Pointer<Utf8> Function(Pointer<Utf8>);
typedef GreetingFunctionFFI = Pointer<Utf8> Function(Pointer<Utf8>);

final GreetingFunction rustGreeting = greeterNative
    .lookup<NativeFunction<GreetingFunctionFFI>>("rust_greeting")
    .asFunction();

String callFFI() {
    final nameStr = "John Smith";
    final Pointer<Utf8> namePtr = Utf8.toUtf8(nameStr);
    print("- Calling rust_greeting with argument: $namePtr");

    final Pointer<Utf8> resultPtr = rustGreeting(namePtr);
    print("- Result pointer: $resultPtr");

    final String greetingStr = Utf8.fromUtf8(resultPtr);
    print("- Response string: $greetingStr");

    return greetingStr;
}
