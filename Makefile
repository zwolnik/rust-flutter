AARCH64        = aarch64-linux-android
ARMV7          = armv7-linux-androideabi
ARMV7A         = armv7a-linux-androideabi
I686           = i686-linux-android
AARCH64_LINKER = ${ANDROID_NDK_ROOT}/toolchains/llvm/prebuilt/linux-x86_64/bin/${AARCH64}26-clang
ARMV7_LINKER   = ${ANDROID_NDK_ROOT}/toolchains/llvm/prebuilt/linux-x86_64/bin/${ARMV7A}26-clang
I686_LINKER    = ${ANDROID_NDK_ROOT}/toolchains/llvm/prebuilt/linux-x86_64/bin/${I686}26-clang
RUST_OUT_DIR        = rust/target
RUST_SONAME         = libgreeter.so
RUST_TARGET_FFI     = ${RUST_OUT_DIR}/greeter.h
RUST_TARGET_REL     = ${RUST_OUT_DIR}/release/${RUST_SONAME}
RUST_TARGET_DBG     = ${RUST_OUT_DIR}/debug/${RUST_SONAME}
RUST_TARGET_AARCH64 = ${RUST_OUT_DIR}/${AARCH64}/release/${RUST_SONAME}
RUST_TARGET_ARMV7   = ${RUST_OUT_DIR}/${ARMV7}/release/${RUST_SONAME}
RUST_TARGET_I686    = ${RUST_OUT_DIR}/${I686}/release/${RUST_SONAME}
FLUTTER_TARGET_FFI  = lib/generated_bindings.dart
FLUTTER_TARGET_REL  = build/app/outputs/flutter-apk/app-release.apk

.PHONY: ffi
ffi: ffi-flutter

.PHONY: ffi-rust
ffi-rust: ${RUST_TARGET_FFI}
${RUST_TARGET_FFI}:
	mkdir -p rust/target
	cd rust && cbindgen ./src/lib.rs -c cbindgen.toml | grep -v \#include | uniq > target/greeter.h

.PHONY: ffi-flutter
ffi-flutter: ffi-rust ${FLUTTER_TARGET_FFI}
${FLUTTER_TARGET_FFI}:
	flutter pub get
	flutter pub run ffigen

.PHONY: check
check:
	cd rust && cargo check

.PHONY: build
build: build-flutter

.PHONY: build-rust
build-rust: build-rust-rel build-rust-aarch64 build-rust-armv7 build-rust-i686

.PHONY: build-rust-rel
build-rust-rel: ${RUST_TARGET_REL}
${RUST_TARGET_REL}:
	cd rust && cargo build --release

.PHONY: build-rust-dbg
build-rust-dbg: ${RUST_TARGET_DBG}
${RUST_TARGET_DBG}:
	cd rust && cargo build

.PHONY: build-rust-aarch64
build-rust-aarch64: ${RUST_TARGET_AARCH64}
${RUST_TARGET_AARCH64}:
	cd rust && CARGO_TARGET_AARCH64_LINUX_ANDROID_LINKER=${AARCH64_LINKER} \
		cargo build --release --target ${AARCH64}

.PHONY: build-rust-armv7
build-rust-armv7: ${RUST_TARGET_ARMV7}
${RUST_TARGET_ARMV7}:
	cd rust && CARGO_TARGET_ARMV7_LINUX_ANDROIDEABI_LINKER=${ARMV7_LINKER} \
		cargo build --release --target ${ARMV7}

.PHONY: build-rust-i686
build-rust-i686: ${RUST_TARGET_I686}
${RUST_TARGET_I686}:
	cd rust && CARGO_TARGET_I686_LINUX_ANDROID_LINKER=${I686_LINKER} \
		cargo build --release --target ${I686}

.PHONY: build-flutter
build-flutter: ffi build-rust ${FLUTTER_TARGET_REL}
${FLUTTER_TARGET_REL}:
	flutter build apk

.PHONY: test
test: test-rust test-flutter

.PHONY: test-rust
test-rust:
	cd rust && cargo test

.PHONY: test-flutter
test-flutter: ffi build-rust-dbg
	flutter test

.PHONY: clean
clean:
	rm -f ${RUST_TARGET_FFI} ${FLUTTER_TARGET_FFI} || true
	flutter clean
	cd rust && cargo clean
